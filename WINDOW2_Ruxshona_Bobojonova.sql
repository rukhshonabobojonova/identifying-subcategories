WITH SubcategorySales AS (
    SELECT
        p.prod_subcategory,
        t.calendar_year,
        SUM(s.amount_sold) AS total_sales
    FROM
        sales s
        JOIN products p ON s.prod_id = p.prod_id
        JOIN times t ON s.time_id = t.time_id
    WHERE
        t.calendar_year BETWEEN 1998 AND 2001
    GROUP BY
        p.prod_subcategory, t.calendar_year
),
PreviousYearSales AS (
    SELECT
        p.prod_subcategory,
        t.calendar_year + 1 AS prev_year,
        SUM(s.amount_sold) AS prev_year_sales
    FROM
        sales s
        JOIN products p ON s.prod_id = p.prod_id
        JOIN times t ON s.time_id = t.time_id
    WHERE
        t.calendar_year BETWEEN 1997 AND 2000
    GROUP BY
        p.prod_subcategory, t.calendar_year + 1
)
SELECT DISTINCT
    sub.prod_subcategory
FROM
    SubcategorySales sub
    JOIN PreviousYearSales prev ON sub.prod_subcategory = prev.prod_subcategory
WHERE
    sub.total_sales > prev.prev_year_sales;
